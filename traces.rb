# coding: utf-8
require 'ipaddr'

class RawAnswer
  attr_accessor :id, :submitter, :conn_type_id, :type, :isp, :submit_ip,
                :submit_time, :report, :state, :state_id, :traces
  # Initialize with the full hash received from the raw_answers table in the DB
  def initialize(data)
    %w(id submitter conn_type_id type isp submit_ip submit_time report
       state state_id).each do |fld|
      self.send('%s='%fld, data[fld])
    end

    # Build the list of individual traces
    res = []
    # This is, of course, built ad-hoc, following the result sets we
    # got.
    delims = ['traceroute to', 'Tracing route to', 'Traza a la direcci¢n']
    delims.select {|d| report =~ /#{d}/}.each do |delim|
      res << report.split(/#{delim}/).reject {|item| item == delim}.
               reject {|item| item.nil? or item.empty?}
    end
    @traces = res.flatten.map {|t| Trace.new(t)}
  end

  def num_traces
    @traces.size
  end

  def num_success_traces
    @traces.select {|t| t.success?}.size
  end

  def success_rate
    return 0 if num_traces == 0
    return num_success_traces.to_f / num_traces
  end

  def success_level
    # What is the success rate of all of the traces for this probe?
    #
    # If zero traces were reported → failure.
    #
    #       x < 25% → failure
    # 25% ≤ x < 50% → lowish
    # 50% ≤ x < 75% → highish
    # 75% ≤ x       → success
    #
    # Of course, reported as a symbol :-]
    rate = success_rate
    return :failure if num_traces == 0
    return :failure if rate < 0.25
    return :lowish  if rate < 0.5
    return :highish if rate < 0.75
    return :success
  end
end

class Trace
  attr_accessor :hops, :dest
  def initialize(str)
    lines = str.split(/\n/)
    dest_line = lines.shift
    dest_line =~ /(\d+\.\d+\.\d+\.\d+)/ && @dest = $1
    puts 'initilizing → %s' % dest
    @hops = lines.map {|l| Hop.new(l, @dest)}
  end

  def success?
    return nil if hops.nil? or hops.empty?
    hops.last.success?
  end

  def failure?
    ! success?
  end
end

class Hop
  attr_accessor :num, :result
  RFC1918 = [IPAddr.new('10.0.0.0/8'), IPAddr.new('172.16.0.0/12'), IPAddr.new('192.168.0.0/16')]
  def initialize(str, dest)
    str =~ /^\s*(\d+)\s*(.*)/ or return nil
    @dest = IPAddr.new(dest)
    @num = $1.to_i
    @result = $2
  end

  def failure?
    ! success?
  end

  def success?
    @result =~ /([\d\.]+)\s*ms/ and result.include?(@dest.to_s)
  end

  def nat?
    return true if RFC1918.select{|net| net.include?(@dest)}
  end
end
