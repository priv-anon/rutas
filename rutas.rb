$: << '.'
require 'sinatra'
require 'haml'
require 'pg'
require 'yaml'
require 'cgi'
require 'time'
require 'traces'

class RutasDB
  def initialize(conf)
    $db = PG::Connection.new(conf) or
      raise RuntimeError, 'Error conectabdo con la BD'
    $db.prepare('ins_record', 'INSERT INTO raw_answers ' +
                              '(submitter, conn_type_id, isp, state_id, ' +
                              'report, submit_ip) VALUES ($1, $2, $3, $4, $5, $6)')
    $db.prepare('get_all', 'SELECT r.id, r.submitter, ct.name AS type, r.isp, ' +
                           's.name AS state, r.submit_ip, r.submit_time, r.report ' +
                           'FROM raw_answers r ' +
                           'JOIN conn_type ct ON (r.conn_type_id=ct.id) ' +
                           'LEFT JOIN states s ON (r.state_id=s.id) ' +
                           'ORDER BY id')
    $db.prepare('get_conn_types', 'SELECT id, name FROM conn_type ORDER BY id')
    $db.prepare('get_states', 'SELECT id, name FROM states ORDER BY id')
    $db.prepare('get_report_by_id', 'SELECT * FROM raw_answers WHERE id = $1')
  end
end

def h(str)
  str='' if str.nil?
  raise Exception unless str.is_a?(String)
  CGI.escapeHTML(str)
end
configure do
  RutasDB.new(YAML.load_file('./config/dbconf.yaml'))
end

# Static documents
get '/style.css' do
  send_file('public/style.css', :content_type => 'text/css', :disposition => 'inline')
end

# General listing (/all), specific report dump (/report/id)

get '/all' do
  @types = $db.exec_prepared('get_conn_types')

  # res is a PG::Result; we copy its data into hashes, to make it
  # easier to query+modify
  @res = $db.exec_prepared('get_all').map {|ans| RawAnswer.new(ans) }
  # @res = []
  # res.each {|row| @res << row}

  # @res.each do |row|
  #   row['num'] = row['report'].split(/\n(traceroute to|Traza a la direcci.n|Tracing route to)/).size
  # end
  haml :full_listing
end

get '/report/:report_id' do
  res = $db.exec_prepared('get_report_by_id', [params[:report_id]])
  if res.num_tuples == 1
    content_type 'text/plain'
    res[0]['report']
  else
    halt 404, '<html><body><h1>Not found</h1></body></html>'
  end
end

# The base entry point, '/', presents the form, and posts it to '/send'.
get '/' do
  @types = $db.exec_prepared('get_conn_types')
  @states = $db.exec_prepared('get_states')
  haml :index
end

post '/send' do
  name = params['submitter'] || ''
  type_id = params['conn_type_id'] # acepta nil
  isp = params['isp'] || ''
  state = params['state_id']
  report = params['report'] || ''
  client = @env.has_key?('HTTP_X_FORWARDED_FOR') ?
             @env['HTTP_X_FORWARDED_FOR'] :
             @env['REMOTE_ADDR']

  res = $db.exec_prepared('ins_record', [name, type_id, isp, state, report, client])

  haml :thanks
end

# Data collection/usage policy
get '/policy' do
  haml :policy
end
