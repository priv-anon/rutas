# Rutas

Sistemita Web para la recolección de información respecto a la censura
de conexiones hacia Tor desde ISPs mexicanos, basado en las rutas
(traceroute).

## ¿Qué y para qué?

Este es un pequeño sistema escrito en [Sinatra](http://sinatrarb.com/)
para ayudarnos a recopilar (y, en un futuro, analizar) las rutas desde
distintos ISPs mexicanos hacia las _autoridades de directorio_ de
Tor.

Uno de los mecanismos más frecuentes empleados por los ISPs para
bloquear la instalación de _relays_ de Tor dentro de su red es el
bloqueo de rutas; éste no (necesariamente) implica el bloqueo de
funcionalidad de cliente Tor, pero sí evita que sus usuarios
participemos activamente en la red.

## ¿Quién?

El sistema fue desarrollado por Gunnar Wolf, responsable del [Proyecto
«Desarrollo de materiales didácticos para los mecanismos de privacidad
y anonimato en redes»](https://priv-anon.unam.mx/) (UNAM/DGAPA/PAPIME
PE102718). Es para uso interno, pero si sirve de algo a alguien más,
¡bienvenidos!
